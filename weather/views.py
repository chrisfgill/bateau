from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from weather import backend

def index(request):
    template = loader.get_template('index.htm')
    return HttpResponse(template.render())

def zipcode(request):
    zipcode = request.GET.get("zipcode")
    if zipcode == None:
        return HttpResponse("zipcode required")
    
    forecast = backend.get_weather(zipcode)
    if forecast == None:
        return HttpResponse("undefined weather api key")
    

    return HttpResponse(forecast["list"])