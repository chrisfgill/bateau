import os
import json
import urllib.request

api_key = os.getenv("WEATHER_API_KEY", "undefined_key")

def get_weather(zipcode):
    if api_key == "undefined_key":
        return None
    url = "http://api.openweathermap.org/data/2.5/forecast/daily?zip={0}&appid={1}".format(zipcode, api_key)
    r = urllib.request.urlopen(url)
    forecast = json.loads(r.read().decode())
    return forecast