from django.db import models
import datetime

class Zipcode(models.Model):
    zipcode = models.CharField(max_length=20)
    date = models.DateTimeField(default=datetime.date.today, blank=True)
    wind = models.IntegerField(default=0)
    rain = models.IntegerField(default=0)
    swell = models.IntegerField(default=0)
